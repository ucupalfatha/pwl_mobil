@extends('layouts.app')

@section('navbar')
    @foreach($dataKategori as $kategori)
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="/home/{{$kategori->id}}">{{$kategori->kategori}}</a>
        </li>
    @endforeach
@endsection

@section('content')

<div class="container">
    <div class="row">
        @foreach($dataMobil as $mobil)
        <div class="col">
            <div class="card mb-3" style="width: 18rem;">
                <img src="{{$mobil->gambar}}" class="card-img-top">
                <div class="card-body">
                    <h4 class="card-title">{{ $mobil->nama }}</h4>
                    <h6 class="card-subtitle mb-2 text-muted">{{ $mobil->kategoris->kategori }}</h6>
                    <p class="card-text">{{ $mobil->deskripsi }}</p>
                    <h7 class="card-subtitle mb-2 text-dark"><strong>{{ $mobil->harga }} Juta</strong></h7>
                    <br>
                    <a href="/keranjang/{{$mobil->id}}" class="btn btn-primary">Buy it</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
            
    
@endsection


