@extends('layouts.app')

@section('navbar')
    @foreach($dataKategori as $kategori)
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="/home/{{$kategori->id}}">{{$kategori->kategori}}</a>
        </li>
    @endforeach
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1>Hasil Transaksi!</h1>
            <h6 class="text-subtitle mb-2 text-muted">Mohon untuk diambil gambar, karena halaman ini hanya akan muncul setelah berhasil transaksi pembelian! Tidak ada di halaman home.</h6>
        </div>
        <div class="col-12 mt-4">
            <table class="table table-dark table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col">No. </th>
                        <th scope="col">Nama </th>
                        <th scope="col">Email </th>
                        <th scope="col">Mobil </th>
                        <th scope="col">Jenis </th>
                        <th scope="col">Harga </th>
                        <th scope="col">Tahun Produksi </th>
                        <th scope="col">Status </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($dataTransaksi as $transaksi)
                        <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td>{{ $transaksi->users->name }}</td>
                            <td>{{ $transaksi->users->email }}</td>
                            <td>{{ $transaksi->mobils->nama }}</td>
                            <td>{{ $transaksi->mobils->kategoris->kategori }}</td>
                            <td>{{ $transaksi->mobils->harga }}</td>
                            <td>{{ $transaksi->mobils->tahun_produksi }}</td>
                            <td>{{ $transaksi->status }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

            
    
@endsection


