@extends('layouts.app')

@section('navbar')
    @foreach($dataKategori as $kategori)
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="/home/{{$kategori->id}}">{{$kategori->kategori}}</a>
        </li>
    @endforeach
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Keranjang</h1>
                <h6 class="card-subtitle mt-2 text-muted">Detail pembelian : </h6>
            </div>
            <div class="col-7 mt-3">
                <img src="{{$dataMobil->gambar}}" class="img-thumbnail">
            </div>
            <div class="col-5">
                <ul class="list-group">
                    <li class="list-group-item list-group-item-info text-center">Detail mobil</li>
                    <li class="list-group-item"><strong>Mobil </strong>{{$dataMobil->nama}}</li>
                    <li class="list-group-item"><strong>Kategori </strong>{{$dataMobil->kategoris->kategori}}</li>
                    <li class="list-group-item"><strong>Tahun Produksi </strong>{{$dataMobil->tahun_produksi}}</li>
                    <li class="list-group-item"><strong>Harga </strong>{{$dataMobil->harga}} Juta</li>
                </ul>
                <ul class="list-group mt-3">
                    <li class="list-group-item list-group-item-info text-center">Detail Anda</li>
                    <li class="list-group-item"><strong>Nama </strong>{{$dataUser->name}}</li>
                    <li class="list-group-item"><strong>Email </strong>{{$dataUser->email}}</li>
                    <li class="list-group-item"><strong>Tanggal </strong><?= date("l, d - F - Y"); ?></li>
                </ul>
                <a href="/sukses/{{$dataMobil->id}}" class="btn btn-primary mt-4">Buy!</a>
            </div>
        </div>
    </div>

@endsection