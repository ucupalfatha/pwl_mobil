@extends('layouts.app')

@section('navbar')
    @foreach($dataKategori as $kategori)
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="/home/{{$kategori->id}}">{{$kategori->kategori}}</a>
        </li>
    @endforeach
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Belanja Sukses!</h1>
                <h6 class="card-subtitle mt-2 text-muted">Detail pembelian : </h6>
            </div>
            <div class="col-7 mt-3">
                <img src="{{$dataTransaksi->mobils->gambar}}" class="img-thumbnail">
            </div>
            <div class="col-5">
                <ul class="list-group">
                    <li class="list-group-item list-group-item-info text-center">Detail mobil</li>
                    <li class="list-group-item"><strong>Mobil </strong>{{$dataTransaksi->mobils->nama}}</li>
                    <li class="list-group-item"><strong>Kategori </strong>{{$dataTransaksi->mobils->kategoris->kategori}}</li>
                    <li class="list-group-item"><strong>Tahun Produksi </strong>{{$dataTransaksi->mobils->tahun_produksi}}</li>
                    <li class="list-group-item"><strong>Harga </strong>{{$dataTransaksi->mobils->harga}} Juta</li>
                </ul>
                <ul class="list-group mt-3">
                    <li class="list-group-item list-group-item-info text-center">Detail Pembeli</li>
                    <li class="list-group-item"><strong>Nama </strong>{{$dataTransaksi->users->name}}</li>
                    <li class="list-group-item"><strong>Email </strong>{{$dataTransaksi->users->email}}</li>
                    <li class="list-group-item"><strong>Tanggal </strong>{{$dataTransaksi->tanggal}}</li>
                </ul>
                <button type="button" class="btn btn-lg btn-success mt-2" disabled>{{$dataTransaksi->status}}</button>
                <a href="/transaksi" class="btn btn-outline-primary">Transaksi</a>
            </div>
        </div>
    </div>

@endsection