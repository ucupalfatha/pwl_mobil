<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mobil extends Model
{
    protected $table = 'mobil';
    protected $primaryKey = 'id';
    use HasFactory;

    public function kategoris(){
        return $this->belongsTo('App\Models\kategori', 'kategori_id');
    }

    public function transaksis(){
        return $this->hasOne('App\Models\transaksi');
    }
}
