<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    protected $table = 'transaksi';
    use HasFactory;

    public function mobils(){
        return $this->belongsTo('App\Models\mobil', 'mobil_id');
    }

    public function users(){
        return $this->belongsTo('App\Models\user');
    }
}
