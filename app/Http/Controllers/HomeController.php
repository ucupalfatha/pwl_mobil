<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\transaksi;
use App\Models\mobil;
use App\Models\kategori;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $dataMobil = mobil::all();
        $dataKategori = kategori::all();


        return view('home', ['dataMobil' => $dataMobil, "dataKategori" => $dataKategori]);
    }

    public function detail(mobil $id)
    {
        $data_user = Auth::user();
        $dataKategori = kategori::all();
        return view('keranjang',["dataUser" => $data_user, "dataMobil" => $id, "dataKategori" => $dataKategori]);
    }


    public function kategori(kategori $id)
    {
        $dataKategori = kategori::all();
        return view('kategori', ["data" => $id, "dataKategori" => $dataKategori]);
    }

    public function sukses(mobil $id)
    {
        $dataKategori = kategori::all();
        $data_user = Auth::user();

        $transaksi = new transaksi;
        $transaksi->tanggal = date("Y-m-d");
        $transaksi->status = "Berhasil";
        $transaksi->users_id = $data_user->id;
        $transaksi->mobil_id = $id->id;

        $transaksi->save();

        return view('sukses', ["dataTransaksi" => $transaksi, "dataKategori" => $dataKategori]);

    }

    public function transaksi(){

        $dataUser = Auth::user();
        $dataTransaksi = transaksi::where('users_id', $dataUser->id)->get();
        $dataKategori = kategori::all();


        return view('transaksi', ["dataTransaksi" => $dataTransaksi, "dataKategori" => $dataKategori, "dataUser" => $dataUser]);
    }
}
