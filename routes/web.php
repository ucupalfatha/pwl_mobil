<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home/{id}', [App\Http\Controllers\HomeController::class, 'kategori'])->name('home');

Route::get('/keranjang/{id}', [App\Http\Controllers\HomeController::class, 'detail'])->name('home');

Route::get('/sukses/{id}', [App\Http\Controllers\HomeController::class, 'sukses'])->name('home');

Route::get('/transaksi', [App\Http\Controllers\HomeController::class, 'transaksi'])->name('home');